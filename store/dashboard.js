import Vue from 'vue'

let privates = {
  DateFormat: function (date) {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    let calcDate = date ? new Date(date) : new Date()
    return months[calcDate.getMonth()] + ' ' + calcDate.getDate() + ' ' + calcDate.getFullYear()
  }
}

export const state = () => ({
  chatList: [],
  myProfile: {},
  chats: []
})

export const getters = {
  chatList (state) {
    return state.chatList
  },
  myProfile (state) {
    return state.myProfile
  },
  chats (state) {
    return state.chats
  }
}

export const mutations = {
  setChatList (state, chatList) {
    state.chatList = chatList
  },
  setMyProfile (state, myProfile) {
    state.myProfile = myProfile
  },
  updateProfileFields (state, { value, key }) {
    Vue.set(state.myProfile, key, value)
  },
  setChats (state, chats) {
    chats.forEach((item, index) => {
      item.formattedDate = privates.DateFormat(item.date)
      item.class = (state.myProfile.id === item.senderId ? 'me' : 'more-chats')
      item.selected = false
    })
    state.chats = chats
  },
  addToChat (state, chat) {
    let myChat = {
      selected: false,
      content: chat,
      formattedDate: privates.DateFormat(),
      class: 'me',
      type: 'text',
      sender: {
        name: state.myProfile.name
      }
    }
    state.chats.push(myChat)
  },
  resetChat(state) {
    state.chats = []
  }
}

export const actions = {
  getChatList ({ commit }) {
    return new Promise((resolve, reject) => {
      this.$axios.$get('https://98bc80ed-04b2-4877-8891-847b031011ef.mock.pstmn.io/get')
        .then(res => {
          console.log(res)
          commit('setChatList', res.data)
          resolve(res)
        })
        .catch(err => {
          console.log(err)
          // console.log(err.response)
          reject(err)
        })
    })
  },

  getMyProfile ({ commit }) {
    return new Promise((resolve, reject) => {
      this.$axios.$get('https://58af4fe0-d8d8-49d1-9771-61c0398fa498.mock.pstmn.io/get')
        .then(res => {
          console.log(res)
          commit('setMyProfile', res.data)
          resolve(res)
        })
        .catch(err => {
          console.log(err)
          // console.log(err.response)
          reject(err)
        })
    })
  },
  editMyProfile ({ commit }, data) {
    return new Promise((resolve, reject) => {
      this.$axios.$post('', data)
        .then(res => {
          console.log(res)
          resolve(res)
        })
        .catch(err => {
          console.log(err)
          // console.log(err.response)
          reject(err)
        })
    })
  },
  updateProfileFields ({ commit }, { value, key }) {
    commit('updateProfileFields', { value, key })
  },

  getChat ({ commit }, chatId) {
    return new Promise((resolve, reject) => {
      // chatId should pass as param in url
      this.$axios.$get('https://26d0943c-2b12-491f-a33c-a87e3b3d6838.mock.pstmn.io/get')
        .then(res => {
          console.log(res)
          commit('setChats', res.data)
          resolve(res)
        })
        .catch(err => {
          console.log(err)
          // console.log(err.response)
          reject(err)
        })
    })
  },
  sendTextReq({commit}, text) {
    return new Promise((resolve, reject) => {
      // this.$axios.$post('', text)
      // should pass object that received from server
      commit('addToChat', text)
      resolve()
    })
  },
  resetChat({commit}) {
    commit('resetChat')
  }
}
