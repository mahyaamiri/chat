export default ({store, redirect}, inject) => {
  inject('encodeContent', (rawContent) => {
    var tagsToReplace = {
      '&': '&amp;',
      '<': '&lt',
      '>': '&gt',
      "'": '&apos;',
      '"': '&quot',
      "?": '&quest;'

    };
    return rawContent.replace(/[&<>'">?]/g, function (tag) {
      return tagsToReplace[tag] || tag;
    })
  })
}
